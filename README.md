# FlashNewsApp

Flash News App untuk Final Project Intensive Coding Bootcamp "Indonesia Mengoding" React-Native Sanbercode Batch 20.

## Spesifikasi Aplikasi

### Konsep Aplikasi

Aplikasi merupakan dumyy app Flash News App yang menampilkan berita-berita terkini dari kanal media online nasional.

Fungsi-fungsi/elemen yang telah dipelajari dan dipergunakan dalam app ini antara lain:
1. `{View, Text, TextInput, Image, ImageBackground, TouchableOpacity, ScrollView, StyleSheet}`.
2. `{useState, useEffect, dan useCallback}`. Selain itu juga digunakan `{useNavigation}`.
3. Arrow Function, Callback, Promise, Function Component.
4. Stack Navigator dan bottom Tab Navigator.
5. Material Community Icons.
6. Loader animation dan Modal.

### API yang digunakan

API publik yang digunakan yaitu sebagaimana dijelaskan [di sini](https://github.com/rizki4106/cnnindonesia-news-api) yaitu [API berita nasional CNN Indonesia](https://www.news.developeridn.com/) yang dapat diakses tanpa `apiKey`.

### Mockup Figma

Akses [Mockup Figma aplikasi ini di sini.](https://www.figma.com/proto/E0CWJAqkzUkntl3SMY91NR/NewsApp?node-id=1%3A55&viewport=-772%2C249%2C0.589565098285675&scaling=scale-down)

### Repository

Akses Repository:
1. Branch [Development](https://gitlab.com/febrifahmi/flashnewsapp/-/tree/development)
2. Branch [Master](https://gitlab.com/febrifahmi/flashnewsapp/-/tree/master)

### File APK

File Expo: [di sini.](https://expo.io/@febrifahmi/projects/flashnewsapp)
File APK: [di sini.](https://drive.google.com/file/d/1wb5vVOX4xi0J17-K5SHvee82g3bzy5qm/view?usp=sharing)

### Video Demo Aplikasi

Video demo app: [di sini.](https://youtu.be/uKxhO8t2H2s)