import React from 'react'
import { StyleSheet, Text, TextInput, View, Image, TouchableOpacity, Animated } from 'react-native'

const LoginTextInput = (props) => {
    return (
        <TextInput
            {...props} // Inherit any props passed to it
            editable
            maxLength={40}
        />
    );
}

const LoginScreen = ({navigation}) => {

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.logoText}>Flash News</Text>
                <Image source={{ uri: "https://pngimg.com/uploads/lightning/lightning_PNG52.png" }} style={styles.logoImg} />
                <Text style={styles.logoTextLead}>Berita Cepat, Berita Akurat</Text>
            </View>
            
                <View style={styles.footer}>
                    <Text style={styles.footerText}>Informasi secepat kilat!</Text>
                    <LoginTextInput
                        placeholder="username"
                        style={styles.loginTextInput}
                    />
                    <LoginTextInput
                        placeholder="password"
                        style={styles.loginTextInput}
                    />
                    <View style={styles.btnRow}>
                        <TouchableOpacity style={styles.btnRegister} onPress={ () => navigation.navigate('Register') } >
                            <Text style={styles.registerText}>Register</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnSignIn} onPress={ () => navigation.navigate('Home') } >
                            <Text style={styles.signInText}>SignIn</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            
        </View>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E3F2FF',
        flex: 1,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 3,
    },
    footer: {
        backgroundColor: '#2E9BFF',
        width: '100%',
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        flex: 2,
    },
    logoText: {
        fontFamily: 'sans-serif',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#2E9BFF',
    },
    logoTextLead: {
        fontFamily: 'sans-serif',
        fontSize: 14,
        color: '#2E9BFF',
    },
    logoImg: {
        width: 140,
        height: 140,
        margin: 10,
    },
    footerText: {
        fontFamily: 'sans-serif',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
        marginTop: 30,
        marginLeft: 30,
    },
    loginTextInput: {
        width: 320,
        height: 45,
        backgroundColor: 'white',
        marginLeft: 30,
        marginTop: 20,
        borderRadius: 5,
    },
    btnRow: {
        marginTop: 30,
        marginHorizontal: 40,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    btnSignIn: {
        backgroundColor: '#F2F3F4',
        width: 150,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
    },
    btnRegister: {
        width: 100,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
    },
    signInText: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#808B96',
    },
    registerText: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    }
})
