import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, ScrollView, useWindowDimensions, Image } from 'react-native';
import HTML from "react-native-render-html";

import LoaderScreen from './loader';

const DetailNewsScreen = ({ route, navigation }) => {

    const { judul, link, poster, tipe, waktu } = route.params;

    const [loading, setLoading] = useState(true);

    const [hasError, setErrors] = useState(false);
    const [detail, setDetail] = useState('');

    const contentWidth = useWindowDimensions().width;

    const regex = /<p>(.*?)<\/p>/;

    const fullArticle = [];

    async function fetchData() {
        const res = await fetch(link)
            .then(res => res.text())
            .then(res => regex.exec(res)[0] )
            .then(res => setDetail(res))
            .catch(err => setErrors(err)
            );
    }

    // alert(detail);

    useEffect(() => {
        fetchData();
        setTimeout(() => setLoading(false), 1000)
    }, []);

    const [refreshing, setRefreshing] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));
    }, []);

    const linkGambar = String(poster); 
    const gambarBig = linkGambar.slice(0, linkGambar.length - 10);
    const revLinkGambar = gambarBig + "w=400&q=90"

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.breadcrumb}>
                    <Text style={styles.teksTebal}>Home {`>`}</Text>
                    <Text style={styles.teksTebalWarna}> News Detail</Text>
                </View>
            </View>
            <View style={styles.batas}>

            </View>
            <View style={styles.body}>
                <ScrollView>
                    <Text style={styles.judulNews}>{judul}</Text>
                    <Image source={{ uri: revLinkGambar }} style={styles.detailImg} />
                    <Text style={styles.publishedAt}>Ditayangkan: {waktu}</Text>
                    <View style={styles.isiNews} >
                        <HTML source={{ html: detail }} contentWidth={contentWidth} />
                        <Text style={styles.referensi}>Source: {link}</Text>
                    </View>
                    <Text></Text>
                </ScrollView>
            </View>
        </View>
    )
}

export default DetailNewsScreen;

const styles = StyleSheet.create({
    container: {
        marginTop: 28,
        width: '100%',
        alignItems: 'center',
        flex: 1,
    },
    header: {
        width: '100%',
        height: 0,
    },
    breadcrumb: {
        height: 30,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#808B96',
        borderBottomWidth: 0.2,
        backgroundColor: 'white',
        elevation: 3,
    },
    teksTebal: {
        fontWeight: '800',
        color: '#808B96',
    },
    teksTebalWarna: {
        fontWeight: '800',
        color: '#2E9BFF',
    },
    batas: {
        height: 35,
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
    },
    judulNews: {
        marginTop: 10,
        paddingBottom: 10,
        marginHorizontal: 10,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#2E9BFF',
        borderBottomColor: '#808B96',
        borderBottomWidth: 0.2,
    },
    detailImg: {
        marginTop: 20,
        marginBottom: 10,
        width: '100%',
        height: 300,
    },
    publishedAt: {
        marginHorizontal: 15,
        fontSize: 12,
        color: '#808B96',
        marginBottom: 10,
    },
    isiNews: {
        marginHorizontal: 15,
    },
    referensi: {
        fontSize: 11,
        fontStyle: 'italic',
        color: '#808B96',
    }
});