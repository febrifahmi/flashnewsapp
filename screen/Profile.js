import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const DATA = [
    {
        id: 1,
        activity: 'Maem bareng Zuckerberg..',
        time: '2 minggu lalu'
    },
    {
        id: 2,
        activity: 'Nonton Ipin Upin bareng anak..',
        time: '1 minggu lalu'
    },
    {
        id: 3,
        activity: 'Helping Bambang find a job at tech company..',
        time: '5 hari lalu'
    },
    {
        id: 4,
        activity: 'Belajar React lumayan bisa lah',
        time: '2 jam lalu'
    },
    {
        id: 5,
        activity: 'Moved to SF to work for Google',
        time: '1 jam lalu'
    },
    {
        id: 6,
        activity: 'Maem Indomie',
        time: 'now'
    },
]

const ActivityItem = ({id, activity, time}) => {
    return(
        <View style={styles.activityDiv}>
            <Text style={styles.activitynya}>{activity}</Text>
            <Text style={styles.timestampnya}>&#9737; {time}</Text>
        </View>
    )
}


const ProfileScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.breadcrumb}>
                    <Text style={styles.teksTebal}>Home {`>`}</Text>
                    <Text style={styles.teksTebalWarna}> Profil</Text>
                </View>
                <View style={styles.profileHeader}>
                    <Image source={{ uri: "https://pbs.twimg.com/profile_images/2661212504/dc29df654d8957df954db40db2a50d7f_400x400.jpeg" }} style={styles.profileImg} />
                    <View>
                        <Text style={styles.profileText}>Febri Fahmi H</Text>
                        <Text style={styles.profileLoc}>Semarang, Indonesia</Text>
                        <Text></Text>
                        <Text style={styles.profileAbout}>Pegawe nyambi, architecture graduates, passionate to learn code</Text>
                    </View>
                </View>
            </View>
            <View style={styles.body}>
                <Text style={styles.activityTitle}>Activity</Text>
                <ScrollView>
                    {DATA.map( item => {
                        return <ActivityItem key={item.id} activity={item.activity} time={item.time} />
                    }).reverse()}
                </ScrollView>
            </View>
        </View>
    )
}

export default ProfileScreen;

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        width: '100%',
        flex: 1,
    },
    header: {
        width: '100%',
        flex: 2,
    },
    breadcrumb: {
        height: 30,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#808B96',
        borderBottomWidth: 0.2,
    },
    teksTebal: {
        fontWeight: '800',
        color: '#808B96',
    },
    teksTebalWarna: {
        fontWeight: '800',
        color: '#2E9BFF',
    },
    profileHeader: {
        width: '100%',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'whitesmoke',
        alignItems: 'center'
    },
    profileImg: {
        width: 100,
        height: 100,
        margin: 20,
        borderRadius: 50,
        marginLeft: 30,
    },
    profileText: {
        fontWeight: '600',
        fontSize: 18,
        color: '#2E9BFF',
        alignItems: 'flex-start',
    },
    profileLoc: {
        fontSize: 12,
        fontWeight: '100',
        color: '#808B96',
    },
    profileAbout: {
        marginRight: 150,
        fontSize: 10,
        fontStyle: 'italic',
        fontWeight: '100',
        color: '#808B96',
    },
    body: {
        width: '100%',
        flex: 5,
        backgroundColor: 'white',
    },
    activityTitle: {
        fontSize: 22,
        fontWeight: '800',
        color: '#2E9BFF',
        marginLeft: 20,
        marginTop: 20,
    },
    activityDiv: {
        marginHorizontal: 30,
        marginVertical: 10,
        padding: 10,
        height: 'auto',
        flex: 1,
        borderLeftColor: '#808B96',
        borderLeftWidth: 0.2,
    },
    activitynya: {
        fontSize: 14,
        fontWeight: '400',
        color: '#808B96'
    },
    timestampnya: {
        fontSize: 10,
        fontWeight: '100',
        color: '#808B96',
        justifyContent: 'flex-end',
        flex: 1,
    }
});