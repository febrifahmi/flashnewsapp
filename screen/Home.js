import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import FeedScreen from './Feed';
import AllNewsScreen from './AllNews';
import ExportScreen from './Export';
import ProfileScreen from './Profile';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen
                name="Feed"
                component={FeedScreen}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                      < MaterialCommunityIcons name="home" color={color} size={size} />
                    )
                }}
            />
            <Tab.Screen
                name="AllNews"
                component={AllNewsScreen}
                options={{
                    tabBarLabel: 'All News',
                    tabBarIcon: ({ color, size }) => (
                      < MaterialCommunityIcons name="format-list-text" color={color} size={size} />
                    )
                }}
            />
            {/* <Tab.Screen 
                name="Export"
                component={ExportScreen}
                options={{
                    tabBarLabel: 'Export',
                    tabBarIcon: ({ color, size }) => (
                      < MaterialCommunityIcons name="export" color={color} size={size} />
                    )
                }}
            /> */}
            <Tab.Screen 
                name="Profile"
                component={ProfileScreen}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                      < MaterialCommunityIcons name="account-box" color={color} size={size} />
                    )
                }}
            />
        </Tab.Navigator>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }
});
