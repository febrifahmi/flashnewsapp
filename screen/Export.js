import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const ExportScreen = () => {
    return (
        <View style={styles.container}>
            <Text>Export Berita</Text>
            <TouchableOpacity style={styles.tombolEkspor}>
                <Text style={styles.teksEkspor}>Export</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ExportScreen;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white'
    },
    tombolEkspor: {
        width: 200,
        height: 50,
        backgroundColor: '#F2F3F4',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    teksEkspor: {
        fontWeight: '600',
        fontSize: 16,
    }
});