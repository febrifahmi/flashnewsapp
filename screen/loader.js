import React, { useState } from 'react'
import { View, Text, Image, Modal, StyleSheet } from 'react-native'

const LoaderScreen = () => {

    const [modalVisible, setModalVisible] = useState(true);

    return (
        <View style={StyleSheet.container}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                presentationStyle='formSheet'
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
                style={styles.modalStyle}
            >
                <Image source={{ uri: "https://thumbs.gfycat.com/BitterEarnestBeardeddragon-small.gif" }} style={styles.modalImage} />
            </Modal>
        </View>
    )
}

export default LoaderScreen;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalStyle: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    modalImage: {
        width: 30,
        height: 30,
    }
})