import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, ScrollView, FlatList, StyleSheet, Image, RefreshControl, TouchableOpacity, Modal, ActivityIndicator } from 'react-native'
import { useNavigation } from '@react-navigation/native';
// import { TouchableOpacity } from 'react-native-gesture-handler';

import DetailNewsScreen from './DetailNews';
import LoaderScreen from './loader';

const Artikel = () => {

    const navigation = useNavigation();

    const [loading, setLoading] = useState(true);

    const [hasError, setErrors] = useState(false);
    const [artikel, setArtikel] = useState([]);

    async function fetchData() {
        const res = await fetch("https://www.news.developeridn.com/");
        res
            .json()
            .then(res => setArtikel(res.data))
            .catch(err => setErrors(err));
    }

    useEffect(() => {
        fetchData();
        setTimeout(() => setLoading(false), 1000)
    }, []);

    const [refreshing, setRefreshing] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));
    }, []);

    const ArtikelItem = ({ judul, link, poster, tipe, waktu }) => {

        const linkGambar = String(poster);
        const gambarBig = linkGambar.slice(0, linkGambar.length - 10);
        const revLinkGambar = gambarBig + "w=400&q=90"

        return (
            <View style={styles.artikelItem} >
                <Image source={{ uri: revLinkGambar }} style={styles.artikelImage} />
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Detail', {
                        judul,
                        link,
                        poster,
                        tipe,
                        waktu,
                    }
                    )
                }} >
                    <Text style={styles.artikelJudul}>{judul}</Text>
                </TouchableOpacity>
                <Text style={styles.artikelTimestamp}>{waktu}</Text>
            </View>
        )
    };

    return (
        <View style={styles.container}>
            {loading === false ? (
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                >
                    {/* <Text>{JSON.stringify(artikel)}</Text>
                <ArtikelItem title={artikel.judul} /> */}
                    {artikel.slice(0, 5).map(item => {
                        return <ArtikelItem judul={item.judul} link={item.link} poster={item.poster} waktu={item.waktu} />
                    })}
                </ScrollView>
            ) : (
                    <LoaderScreen style={styles.loadingScreen} />
                )}
        </View>
    )
}

export default Artikel;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        flex: 1,
    },
    artikelImage: {
        width: '100%',
        height: 200,
        flex: 1,
        borderRadius: 8,
    },
    artikelJudul: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#2C3E50',
        justifyContent: 'flex-start',
        marginVertical: 5,
    },
    artikelTimestamp: {
        fontSize: 9,
        fontWeight: 'bold',
        color: '#808B96',
        justifyContent: 'flex-start',
        marginVertical: 5,
    },
    artikelItem: {
        width: '100%',
        height: 300,
        marginVertical: 10,
        flex: 1,
        flexDirection: 'column',
        borderBottomColor: '#808B96',
        borderBottomWidth: 0.2,
    },
    loadingScreen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
