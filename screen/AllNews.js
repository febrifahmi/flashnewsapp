import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, StyleSheet, ScrollView, RefreshControl, Image, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native';

import LoaderScreen from './loader';

const AllNewsScreen = () => {

    const navigation = useNavigation();

    const [loading, setLoading] = useState(true);

    const [hasError, setErrors] = useState(false);
    const [artikel, setArtikel] = useState([]);

    async function fetchData() {
        const res = await fetch("https://www.news.developeridn.com/");
        res
            .json()
            .then(res => setArtikel(res.data))
            .catch(err => setErrors(err));
    }

    useEffect(() => {
        fetchData();
        setTimeout(() => setLoading(false), 1000)
    }, []);

    const [refreshing, setRefreshing] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));
    }, []);

    const ArtikelItem = ({ judul, link, poster, tipe, waktu }) => {
        return (
            <View style={styles.artikelItem} >
                <View style={styles.leftSide}>
                    <Image source={{ uri: `${poster}` }} style={styles.artikelImage} />
                </View>
                <View style={styles.rightSide}>
                    <TouchableOpacity onPress={() => {
                        navigation.navigate('Detail', {
                            judul: judul,
                            link: link,
                            poster: poster,
                            tipe: tipe,
                            waktu: waktu,
                        })
                    }} >
                        <Text style={styles.artikelJudul}>{judul}</Text>
                    </TouchableOpacity>
                    <Text style={styles.artikelTimestamp}>&#9737; {waktu}</Text>
                </View>
            </View>
        )
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.breadcrumb}>
                    <Text style={styles.teksTebal}>Home {`>`}</Text>
                    <Text style={styles.teksTebalWarna}> All News</Text>
                </View>
            </View>
            <View style={styles.batas}>

            </View>
            {loading === false ? (
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                >
                    {artikel.map(item => {
                        return <ArtikelItem judul={item.judul} link={item.link} poster={item.poster} waktu={item.waktu} />
                    })}
                </ScrollView>
            ) : (
                    <LoaderScreen style={styles.loadingScreen} />
                )}
        </View>
    )
}

export default AllNewsScreen;

const styles = StyleSheet.create({
    container: {
        marginTop: 28,
        width: '100%',
        alignItems: 'center',
        flex: 1,
    },
    header: {
        width: '100%',
        flex: 2,
    },
    breadcrumb: {
        height: 30,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#808B96',
        borderBottomWidth: 0.2,
        backgroundColor: 'white',
        elevation: 3,
    },
    teksTebal: {
        fontWeight: '800',
        color: '#808B96',
    },
    teksTebalWarna: {
        fontWeight: '800',
        color: '#2E9BFF',
    },
    batas: {
        height: 35,
    },
    artikelItem: {
        height: 80,
        backgroundColor: 'white',
        marginVertical: 5,
        marginHorizontal: 10,
        flex: 1,
        flexDirection: 'row',
        borderRadius: 8,
    },
    leftSide: {
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    rightSide: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignContent: 'space-between',
        marginHorizontal: 5,
    },
    artikelImage: {
        width: 100,
        height: 80,
        marginRight: 5,
        borderRadius: 5,
    },
    artikelJudul: {
        marginTop: 8,
        fontSize: 13,
        fontWeight: 'bold',
        width: 250,
        alignItems: 'center'
    },
    artikelTimestamp: {
        fontSize: 10,
        fontWeight: '300',
        alignItems: 'flex-end'
    },
    loadingScreen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})