import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image, ImageBackground } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from '@react-navigation/stack';

import Artikel from './Artikel';
import DetailNewsScreen from './DetailNews';

const SearchTextInput = (props) => {
    return (
        <TextInput
            {...props} // Inherit any props passed to it
            editable
            maxLength={40}
        />
    );
};

const Feeds = () => {

    return (
        <View style={styles.container}>
            <View style={styles.headerPart}>
                <ImageBackground source={{ uri: "https://pngimg.com/uploads/reporter/reporter_PNG22.png" }} style={styles.bgHeader}>
                    <SearchTextInput
                        placeholder="cari berita"
                        clearTextOnFocus
                        style={styles.searchTextInput}
                    />
                    <TouchableOpacity style={styles.searchBtn}>
                        <Image source={{ uri: "https://lh3.googleusercontent.com/proxy/WAK--SPJYyGoc57kBEwO1KVjx0Moq--qtq3XDeEF5mJQQqrUsYhhB79wAel0WPKuEpiKrXCZYG8G-G3_6Roi6ANwOWK_qnes3tw5vyo2q8gX2UluH8y-DHgMluPoQHxDkCdBzFJlOUIkWFf8Wg" }} style={styles.searchIcon} />
                    </TouchableOpacity>
                </ImageBackground>
            </View>
            <View style={styles.bodyPart}>
                <Artikel />
                {/* <Text>(Home) Feed Screen</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text>Menuju ke Login Screen</Text>
                </TouchableOpacity> */}
            </View>
        </View>
    )
}

const FeedStack = createStackNavigator();

const FeedScreen = () => {
    return (
        <FeedStack.Navigator
            initialRouteName="Feed"
            screenOptions={{
                headerShown: false
            }}
        >
            <FeedStack.Screen name="Feed" component={Feeds} />
            <FeedStack.Screen name="Detail" component={DetailNewsScreen} />
        </FeedStack.Navigator>
    )
};

export default FeedScreen;


const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E3F2FF',
        flex: 1,
    },
    headerPart: {
        width: '100%',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        elevation: 5,
        shadowColor: 'grey',
        shadowOffset: { width: 2, height: 5,},
        shadowOpacity: 0.5,
    },
    bgHeader: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
    },
    bodyPart: {
        width: '100%',
        paddingHorizontal: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 5,
    },
    searchTextInput: {
        height: 45,
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: '#AED6F1',
        flex: 7,
    },
    searchBtn: {
        width: 45,
        height: 45,
        backgroundColor: '#2E9BFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flex: 1,
    },
    searchIcon: {
        width: 24,
        height: 24,
    }
});
